package it.unibo.oop.lab.reactivegui03;

import java.util.concurrent.TimeUnit;

import it.unibo.oop.lab.reactivegui02.ConcurrentGUI;


public class AnotherConcurrentGUI extends ConcurrentGUI {
    private static final long serialVersionUID = 2L;
    public AnotherConcurrentGUI() {
        super();
        final MyTimer timer = new MyTimer();
        new Thread(timer).start();
    }

    public final class MyTimer extends Thread {
        private static final long MS_TO_SLEEP = 10_000;
        public void run() {
            try {
                TimeUnit.MILLISECONDS.sleep(MS_TO_SLEEP);
                stopAgent();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}



