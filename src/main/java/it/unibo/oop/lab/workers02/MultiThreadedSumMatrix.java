package it.unibo.oop.lab.workers02;

import java.util.ArrayList;
import java.util.List;

public final class MultiThreadedSumMatrix implements SumMatrix {

    private final int nThread;

    /**
     * @param nThread number of threads
     */
    public MultiThreadedSumMatrix(final int nThread) {
	this.nThread = nThread;
    }

    private static class Worker extends Thread {
	private double[][] matrix;
	private int rStart;
	private int cStart;
	private int rFinish;
	private int cFinish;
	private double res;

	/**
	 * Constructor for the worker.
	 * 
	 * @param matrix
	 * @param rStart
	 * @param cStart
	 * @param rFinish
	 * @param cFinish
	 */
	Worker(final double[][] matrix, final int rStart, final int cStart, final int rFinish, final int cFinish) {
	    super();
	    this.matrix = matrix;
	    this.rStart = rStart;
	    this.cStart = cStart;
	    this.rFinish = rFinish;
	    this.cFinish = cFinish;
	    this.res = 0;
	}

	public void run() {
	    System.out.println("Working from (" + this.rStart + ", " + this.cStart + ")" + " to (" + this.rFinish + ", "
	            + this.cFinish + ")");
	    for (int i = this.rStart; i < this.rFinish; i++) {
		for (int j = this.cStart; j < this.cFinish; j++) {
		    res += matrix[i][j];
		}
	    }
	}

	/**
	 * Returns the risult of summing up the integers within the list.
	 * 
	 * @return the sum of every element in the array
	 */
	public double getRes() {
	    return this.res;
	}
    }

    public double sum(final double[][] matrix) {
        final int jump = matrix.length / nThread;
        /*
         * Build a list of workers
         */
        final List<Worker> workers = new ArrayList<>(nThread);
        for (int i = 0; i < nThread - 1; i++) {
            workers.add(new Worker(matrix, i * jump, 0, (i + 1) * jump, matrix[0].length));
        }
        workers.add(new Worker(matrix, (nThread - 1) * jump, 0, matrix.length, matrix[0].length));
        /*
         * Start them
         */
        for (final Worker w: workers) {
            w.start();
        }
        /*
         * Wait for every one of them to finish. This operation is _way_ better done by
         * using barriers and latches, and the whole operation would be better done with
         * futures.
         */
        double sum = 0;
        for (final Worker w: workers) {
            try {
                w.join();
                sum += w.getRes();
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        }
        /*
         * Return the sum
         */
        return sum;
    }
}
